from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime
from math import sqrt

import ctypes
import _ctypes
import sys
import random

from sqlalchemy import (create_engine, Table, Column, Integer,
    String, MetaData, Float)


if sys.hexversion >= 0x03000000:
    import _thread as thread
else:
    import thread



class Kinect2Postgres(object):
    def __init__(self,uniqueKinectId,initTimeStamp):
        self.eng = create_engine('postgresql://postgres:sa@localhost/liftcoach')
        self.metadata = MetaData(self.eng)
        self.initTimeStamp = initTimeStamp
        self.uniqueKinectId = str(uniqueKinectId)
        self.joints = Table('Joints', self.metadata,
                Column('uniqueKinectId', String),
                Column('initTimeStamp', Integer),
                Column('relativeTime', Integer),
                Column('index', Integer),
                Column('trackingState', Integer),
                Column('x', Float),
                Column('y', Float),
                Column('z', Float),
                Column('a', Float),
                Column('b', Float),
                Column('c', Float),
                Column('d', Float),
            )
        self.metadata.create_all(self.eng)

        try:
            create_tables(self.eng)
        except: # need to catch it due to possible invalid positions (with inf)
            pass


    def save_joints(self,body,relativeTime):
        joint_dicts = []
        for index in range(PyKinectV2.JointType_Count):  
           joint_dicts.append(self.joint_to_dict(body.joints[index],body.joint_orientations[index],index,relativeTime))
        self.eng.execute(self.joints.insert().values(joint_dicts))
        

    def joint_to_dict(self,joint,orient,index,reltime):
        jd = {'uniqueKinectId':self.uniqueKinectId,
          'initTimeStamp':self.initTimeStamp,
          'relativeTime':reltime,
          'index':index,
          'trackingState':joint.TrackingState,
          'x':joint.Position.x,
          'y':joint.Position.y,
          'z':joint.Position.z,
          'a':orient.Orientation.x,
          'b':orient.Orientation.y,
          'c':orient.Orientation.z,
          'd':orient.Orientation.w,
          }
        assert index == joint.JointType , 'oops'
        assert index == orient.JointType, 'opps'
        return jd


    def create_tables(eng):


        with eng.connect() as con:
            rs = con.execute("SELECT VERSION()")
            print(rs.fetchone())
            meta = MetaData(eng)

            frames = Table('Frames', meta,
                Column('uniqueKinectId', String),
                Column('initTimeStamp', Integer),
                Column('relativeTime', Integer),
                Column('color', String),
                Column('depth', String),
                Column('body', String),
                Column('bodyIndex', String),
                Column('infrared', String),
            )

            wbb = Table('WiiBalanceBoard', meta,
                Column('l_mac', String),
                Column('r_mac', String),
                Column('initTimeStamp', Integer),
                Column('relativeTime', Integer),
                Column('l_x', Float),
                Column('l_y', Float),
                Column('l_tr', Float),
                Column('l_tl', Float),
                Column('l_tr', Float),
                Column('l_bl', Float),
                Column('r_x', Float),
                Column('r_y', Float),
                Column('r_br', Float),
                Column('r_tl', Float),
                Column('r_tr', Float),
                Column('r_bl', Float),
                Column('r_br', Float),
            )

            lifts = Table('Lifts', meta,
                Column('userID', String),
                Column('liftCoachVersion', Integer),
                Column('uniqueKinectId_0', String),
                Column('uniqueKinectId_1', String),
                Column('l_wbb_mac', String),
                Column('r_wbb_mac', String),
                Column('initTimeStamp', Integer),
                Column('relativeTimeStart', Integer),
                Column('relativeTimeEnd', Integer),
                Column('liftType', String),
                Column('heightMM', Float),
                Column('spanMM', Float),
                Column('userKG', Float),
                Column('barbellKG', Float),
                Column('floor_clipW', Float),
                Column('floor_clipX', Float),
                Column('floor_clipY', Float),
                Column('floor_clipZ', Float),
                Column('l_wbb_x', Float),
                Column('l_wbb_y', Float),
                Column('l_wbb_z', Float),
                Column('r_wbb_x', Float),
                Column('r_wbb_y', Float),
                Column('r_wbb_z', Float),
            )
            lifts.create()


            s = sqlalchemy.select([joints])
            rs = con.execute(s)

            for row in rs:
                print (row)
