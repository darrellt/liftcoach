from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime
from math import sqrt

import numpy as np



JT_SpineBase = 0
JT_SpineMid = 1
JT_Neck = 2
JT_Head = 3
JT_ShoulderLeft = 4
JT_ElbowLeft = 5
JT_WristLeft = 6
JT_HandLeft = 7
JT_ShoulderRight = 8
JT_ElbowRight = 9
JT_WristRight = 10
JT_HandRight = 11
JT_HipLeft = 12
JT_KneeLeft = 13
JT_AnkleLeft = 14
JT_FootLeft = 15
JT_HipRight = 16
JT_KneeRight = 17
JT_AnkleRight = 18
JT_FootRight = 19
JT_SpineShoulder = 20
JT_HandTipLeft = 21
JT_ThumbLeft = 22
JT_HandTipRight = 23
JT_ThumbRight = 24
JT_Count = 25


def calc_distance(joint_points,joint,x,y):
    joint_x = joint_points[joint].x
    joint_y = joint_points[joint].y
    distance=sqrt((joint_x-x)**2+(joint_y-y)**2)
    return distance

def calc_height(joint,fp):
    d,a,b,c = fp
    x,y,z= joint
    height = a*x + b*y + c*z + d
    return height

def clamp(n,lo,hi):
    if n < lo:
        return lo
    if n > hi:
        return hi
    return n


def calc_joint_angle(j):
    '''Takes 3 xyz np arrays and calculates the angle between them'''
    a = j[0]
    b = j[1]
    c = j[2]

    ba = a - b
    bc = c - b

    cosine_angle = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))
    angle = np.arccos(cosine_angle)

    return np.degrees(angle)

def joints_to_angle(joints,j1,j2,j3):
    j = joints_to_np(joints,j1,j2,j3)
    return calc_joint_angle(j)

def joints_to_np(joints,j1,j2,j3):
    a = joint_to_np(joints[j1])
    b = joint_to_np(joints[j2])
    c = joint_to_np(joints[j3])
    return np.array([a,b,c])

def joint_to_np(joint):
    return np.array([joint.Position.x,joint.Position.y,joint.Position.z])

def joint_to_tuple(joint):
    return((joint.Position.x,joint.Position.y,joint.Position.z))

def fp_to_tuple(plane):
    return((plane.w,plane.x,plane.y,plane.z))



class Body(object):
    def __init__(self):


        self.floor_clip_plane  = (0,0,0,0)
        self.left_hand_pos  = (0,0,0)
        self.right_hand_pos  = (0,0,0)
        self.hand_width = []
        self.left_hand_height = []
        self.right_hand_height = []
        self.bar_height = []
        self.left_elb_angle = []
        self.right_elb_angle = []
        self.hip_angle = []
        self.left_knee_angle = []
        self.right_knee_angle = []
        self.timestamps = []





    def draw_body(self, joints, jointPoints, color):

        # Torso
        skull = ( JT_Head, JT_Neck)
        neck = ( JT_Neck, JT_SpineShoulder)
        ( JT_SpineShoulder, JT_SpineMid)
        ( JT_SpineMid, JT_SpineBase)
        ( JT_SpineShoulder, JT_ShoulderRight)
        ( JT_SpineShoulder, JT_ShoulderLeft)
        ( JT_SpineBase, JT_HipRight)
        ( JT_SpineBase, JT_HipLeft)

        # Right Arm
        ( JT_ShoulderRight, JT_ElbowRight)
        ( JT_ElbowRight, JT_WristRight)
        ( JT_WristRight, JT_HandRight)
        ( JT_HandRight, JT_HandTipRight)
        ( JT_WristRight, JT_ThumbRight)

        # Left Arm
        ( JT_ShoulderLeft, JT_ElbowLeft)
        ( JT_ElbowLeft, JT_WristLeft)
        ( JT_WristLeft, JT_HandLeft)
        ( JT_HandLeft, JT_HandTipLeft)
        ( JT_WristLeft, JT_ThumbLeft)

        # Right Leg
        ( JT_HipRight, JT_KneeRight)
        ( JT_KneeRight, JT_AnkleRight)
        ( JT_AnkleRight, JT_FootRight)

        # Left Leg
        ( JT_HipLeft, JT_KneeLeft)
        ( JT_KneeLeft, JT_AnkleLeft)
        ( JT_AnkleLeft, JT_FootLeft)

    def calc_body_angles(self,body):
        joints = body.joints
        self.left_hand_pos = joint_to_tuple(joints[JT_HandLeft])
        self.right_hand_pos = joint_to_tuple(joints[JT_HandRight])
        self.hand_width.append(np.linalg.norm(np.array(self.right_hand_pos)-np.array(self.left_hand_pos)))
        self.left_hand_height.append(calc_height(self.left_hand_pos,self.floor_clip_plane))
        self.right_hand_height.append(calc_height(self.right_hand_pos,self.floor_clip_plane))
        self.bar_height.append((self.right_hand_height[-1]+self.left_hand_height[-1])/2)
        self.left_elb_angle.append(joints_to_angle(joints,JT_ShoulderLeft,JT_ElbowLeft,JT_WristLeft))
        self.right_elb_angle.append(joints_to_angle(joints,JT_ShoulderRight,JT_ElbowRight,JT_WristRight))
        self.left_knee_angle.append(joints_to_angle(joints,JT_HipLeft,JT_KneeLeft,JT_AnkleLeft))
        self.right_knee_angle.append(joints_to_angle(joints,JT_HipRight,JT_KneeRight,JT_AnkleRight))
        self.timestamps.append(self.playtime)





            # --- draw skeletons to _frame_surface
            if self._bodies is not None:
                self.floor_clip_plane = fp_to_tuple(self._bodies.floor_clip_plane)
                for i in range(0, self._kinect.max_body_count):
                    body = self._bodies.bodies[i]
                    if not body.is_tracked:
                        continue
                    self.total_error = 1
                    self.calc_body_angles(body)
                    # self.k2p.save_joints(body,self._kinect._last_body_frame_time)
                    joints = body.joints

                    # convert joint coordinates to color space
                    joint_points = self._kinect.body_joints_to_color_space(joints)
                    self.draw_body(joints, joint_points, SKELETON_COLORS[2])
                    self.last_joint_points = joint_points

                    joint_dicts = []
                    for index in range(JT_Count):
                        joint_dicts.append(self.k2p.joint_to_dict(body.joints[index],body.joint_orientations[index],index,0))
                    self.last_joints = joint_dicts
                    if randint(0,10) == 9:
                        self.heartrate += randint(-1,1)
                    self.heartrate = clamp(self.heartrate, 77, 117)
                    self.messages += "\nHEARTRATE: {}".format(self.heartrate)

            if self.show_debug_info:
                self.messages += "\ntotal_error: {:.0f}".format(self.total_error)

            self.draw_error_bar(self._frame_surface)
            self.draw_messages(self._frame_surface)

            #draw logo
            self._frame_surface.blit(lift_coach_logo, (0,0))
            # --- copy back buffer surface pixels to the screen, resize it if needed and keep a aspect ratio
            # --- (screen size may be different from Kinect's color frame size)
            if False:
                h_to_w = float(self._frame_surface.get_height()) / self._frame_surface.get_width()
                target_height = int(h_to_w * self._screen.get_width())
                surface_to_draw = pygame.transform.scale(self._frame_surface, (self._screen.get_width(), target_height))
                self._screen.blit(surface_to_draw, (0,0))
            else:
                self._screen.blit(self._frame_surface, (0,0))
            surface_to_draw = None
            # pygame.display.set_caption(" LIFT COACH FPS: {0:.2f}  Playtime: {1:.2f} ".format(self._clock.get_fps(), self.playtime))
            # pygame.display.update()

            # --- Go ahead and update the screen with what we've drawn.
            pygame.display.flip()

            # --- Limit to 60 frames per second
            milliseconds = self._clock.tick(60)
            self.playtime += milliseconds / 1000.0

        # Close our Kinect sensor, close the window and quit.
        self._kinect.close()
        pygame.quit()



if __name__ == '__main__':
    # multiprocessing.freeze_support()
    __main__ = "Kinect v2 Body Game"
    game = BodyGameRuntime()
    game.run()

