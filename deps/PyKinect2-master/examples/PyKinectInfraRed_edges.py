from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime
from scipy.stats import threshold
from skimage import data, io, filters
from skimage.feature import blob_dog, blob_log, blob_doh
from math import sqrt
from skimage.color import rgb2gray

import matplotlib.pyplot as plt

import cv2
import ctypes
import _ctypes
import pygame
import sys
import numpy as np


if sys.hexversion >= 0x03000000:
    import _thread as thread
else:
    import thread

# colors for drawing different bodies
SKELETON_COLORS = [pygame.color.THECOLORS["red"],
                   pygame.color.THECOLORS["blue"],
                   pygame.color.THECOLORS["green"],
                   pygame.color.THECOLORS["orange"],
                   pygame.color.THECOLORS["purple"],
                   pygame.color.THECOLORS["yellow"],
                   pygame.color.THECOLORS["violet"]]

def auto_canny(image, sigma=0.33):
	# compute the median of the single channel pixel intensities
	v = np.median(image)
 
	# apply automatic Canny edge detection using the computed median
	lower = int(max(0, (1.0 - sigma) * v))
	upper = int(min(255, (1.0 + sigma) * v))
	edged = cv2.Canny(image, lower, upper)
 
	# return the edged image
	return edged

class InfraRedRuntime(object):

    def __init__(self):
        pygame.init()

        # Used to manage how fast the screen updates
        self._clock = pygame.time.Clock()

        # Loop until the user clicks the close button.
        self._done = False

        # Used to manage how fast the screen updates
        self._clock = pygame.time.Clock()

        # Kinect runtime object, we want only color and body frames
        self._kinect = PyKinectRuntime.PyKinectRuntime(
            PyKinectV2.FrameSourceTypes_Infrared)

        # back buffer surface for getting Kinect infrared frames, 8bit grey,
        # width and height equal to the Kinect color frame size
        self._frame_surface = pygame.Surface(
            (self._kinect.infrared_frame_desc.Width, self._kinect.infrared_frame_desc.Height), 0, 24)
        # here we will store skeleton data
        self._bodies = None

        # Set the width and height of the screen [width, height]
        self._infoObject = pygame.display.Info()
        self._screen = pygame.display.set_mode((self._kinect.infrared_frame_desc.Width, self._kinect.infrared_frame_desc.Height),
                                               pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.RESIZABLE, 32)

        print("infrared res = {}x{}".format(
            self._kinect.infrared_frame_desc.Width, self._kinect.infrared_frame_desc.Height))
        self.infrared_res = (self._kinect.infrared_frame_desc.Width,
                             self._kinect.infrared_frame_desc.Height)
        self.infrared_res = (self._kinect.infrared_frame_desc.Height,
                             self._kinect.infrared_frame_desc.Width)

        self._kinect.
        pygame.display.set_caption("Kinect for Windows v2 Infrared")

    def draw_infrared_frame(self, frame, target_surface):
        if frame is None:  # some usb hub do not provide the infrared image. it works with Kinect studio though
            return
        target_surface.lock()
        f8 = np.uint8(frame.clip(1, 4090) / 16.)
        blur = np.reshape(f8,self.infrared_res) 
        # edges = cv2.Canny(np.reshape(f8,self.infrared_res),256,512,apertureSize = 3)
        # edges = auto_canny(blur)

        # edges = cv2.bitwise_not(blur)
        edges = threshold(blur,0,6)
        edges = edges * 20
        # edges = threshold(edges,240)

        # f8 = threshold(f8, 252)
        # blobs = blob_dog(np.reshape(f8,self.infrared_res), min_sigma=2, max_sigma=50, sigma_ratio=1.6, threshold=1.0, )


        # frame8bit=np.dstack((edges,edges,edges)) 
        frame8bit=np.dstack((f8,edges.flatten(),f8)) 
        address = self._kinect.surface_as_array(target_surface.get_buffer())
        ctypes.memmove(address, frame8bit.ctypes.data, frame8bit.size)
        del address
        target_surface.unlock()
        PyKinectV2.
        cv2.

        # compute radius in third column
        # blobs[:, 2] = blobs[:, 2] * sqrt(2)


    def run(self):
        playtime = 0
        # -------- Main Program Loop -----------
        while not self._done:
            # --- Main event loop
            for event in pygame.event.get():  # User did something
                if event.type == pygame.QUIT:  # If user clicked close
                    self._done = True  # Flag that we are done so we exit this loop

                elif event.type == pygame.VIDEORESIZE:  # window resized
                    self._screen = pygame.display.set_mode(event.dict['size'],
                                                           pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.RESIZABLE, 32)

            # --- Getting frames and drawing
            if self._kinect.has_new_infrared_frame():
                frame = self._kinect.get_last_infrared_frame()
                self.draw_infrared_frame(frame, self._frame_surface)
                frame = None

            self._screen.blit(self._frame_surface, (0, 0))

            text = "FPS: {0:.2f}   Playtime: {1:.2f}".format(
                self._clock.get_fps(), playtime)
            pygame.display.set_caption(text)
            # print self._clock.get_fps()

            pygame.display.update()

            # --- Go ahead and update the screen with what we've drawn.
            pygame.display.flip()

            # --- Limit to 60 frames per second
            milliseconds = self._clock.tick(60)
            playtime += milliseconds / 1000.0

        # Close our Kinect sensor, close the window and quit.
        self._kinect.close()
        pygame.quit()


__main__ = "Kinect v2 InfraRed"
game = InfraRedRuntime()
game.run()
