import sys
import direct.directbase.DirectStart
import vrpn

from direct.showbase.DirectObject import DirectObject
from direct.showbase.InputStateGlobal import inputState

from panda3d.core import AmbientLight
from panda3d.core import DirectionalLight
from panda3d.core import Vec3
from panda3d.core import Vec4
from panda3d.core import Quat
from panda3d.core import Point3
from panda3d.core import TransformState
from panda3d.core import BitMask32
from panda3d.core import LQuaternionf, LVecBase4f, VBase3
from panda3d.physics import ForceNode, LinearVectorForce
from panda3d.bullet import BulletWorld
from panda3d.bullet import BulletBoxShape
from panda3d.bullet import BulletCylinderShape
from panda3d.bullet import BulletSphereShape
from panda3d.bullet import BulletRigidBodyNode
from panda3d.bullet import BulletGenericConstraint
from panda3d.bullet import BulletSphericalConstraint
from panda3d.bullet import BulletSliderConstraint
from panda3d.bullet import BulletDebugNode

import logging
import threading
import time
import multiprocessing
# from read_sensors import worker
import numpy as np



class Game(DirectObject):


  def tracker_callback(self,tname,data):
  if data['sensor'] == 5: #left shoulder
    self.shoulder_quad = data['quaternion']

  if data['sensor'] == 6: #left elbow
    self.elbow_quad = data['quaternion']

  if data['sensor'] == 7: #left wrist
    self.wrist_quad = data['quaternion']

  def __init__(self):
    self.odometry = None
    self.prev_odometry = None
    self.total_time = 0
    self.wrist_quad = (0,0,0,0)
    self.elbow_quad = (0,0,0,0)
    self.shoulder_quad = (0,0,0,0)


    self.tracker=vrpn.receiver.Tracker("Tracker0@10.0.1.7")
    self.tracker.register_change_handler("tracker", self.tracker_callback, "position")


    # game objects to be manipulated by sensor data
    self.wrist = None
    self.elbow = None
    self.shoulder = None
    self.upper_arm = None
    self.forearm = None

    base.setBackgroundColor(0.1, 0.1, 0.8, 1)
    base.setFrameRateMeter(True)
    base.cam.setPos(0, -20, 5)
    base.cam.lookAt(0, 0, 0)

    # Light
    alight = AmbientLight('ambientLight')
    alight.setColor(Vec4(0.5, 0.5, 0.5, 1))
    alightNP = render.attachNewNode(alight)

    dlight = DirectionalLight('directionalLight')
    dlight.setDirection(Vec3(1, 1, -1))
    dlight.setColor(Vec4(0.7, 0.7, 0.7, 1))
    dlightNP = render.attachNewNode(dlight)

    render.clearLight()
    render.setLight(alightNP)
    render.setLight(dlightNP)

    # Input
    self.accept('escape', self.doExit)
    self.accept('r', self.doReset)
    self.accept('f1', self.toggleWireframe)
    self.accept('f2', self.toggleTexture)
    self.accept('f3', self.toggleDebug)
    self.accept('f5', self.doScreenshot)

    # Task
    taskMgr.add(self.update, 'updateWorld')

    # Physics
    self.setup()

  # _____HANDLER_____

  def doExit(self):
    self.cleanup()
    sys.exit(1)

  def doReset(self):
    self.cleanup()
    self.setup()

  def toggleWireframe(self):
    base.toggleWireframe()

  def toggleTexture(self):
    base.toggleTexture()

  def toggleDebug(self):
    if self.debugNP.isHidden():
      self.debugNP.show()
    else:
      self.debugNP.hide()

  def doScreenshot(self):
    base.screenshot('Bullet')

  def doRemove(self, bodyNP, task):
    self.world.removeRigidBody(bodyNP.node())
    bodyNP.removeNode()
    return task.done

  # ____TASK___

  def update(self, task):
    dt = globalClock.getDt()
    self.world.doPhysics(dt, 20, 1.0/180.0)
    self.total_time += dt
    self.tracker.mainloop()

    #  if all([e is not None for e in [self.odometry, self.shoulder, self.prev_odometry]]):
    if True:
      # receive sensor data
      #  upper_arm_quat = self.odometry.odometry["shoulder_gyro"][0].orientation.get_quaternion()
      #  forearm_quat = self.odometry.odometry["elbow_gyro"][0].orientation.get_quaternion()
      #  wrist_quat = self.odometry.odometry["wrist_gyro"][0].orientation.get_quaternion()

      upper_arm_quat = Quat(self.shoulder_quad)
      forearm_quat = Quat(self.shoulder_quad)
      wrist_quat = Quat(self.wrist_quad)

      # forward kinematics
      self.shoulder.setQuat(upper_arm_quat)
      upper_arm_length = 4
      upper_arm_pos = upper_arm_quat.getRight() * -upper_arm_length/2
      self.upper_arm.setQuat(upper_arm_quat)
      self.upper_arm.setPos(upper_arm_pos)

      elbow_pos = upper_arm_quat.getRight() * -upper_arm_length
      self.elbow.setQuat(forearm_quat)
      self.elbow.setPos(elbow_pos)

      self.forearm.setQuat(forearm_quat)
      forearm_length = 4
      forearm_pos = elbow_pos + forearm_quat.getRight() * -forearm_length/2
      self.forearm.setPos(forearm_pos)

      self.wrist.setQuat(wrist_quat)
      wrist_pos = elbow_pos + forearm_quat.getRight() * -forearm_length
      self.wrist.setPos(wrist_pos)
    return task.cont

  def cleanup(self):
    self.worldNP.removeNode()
    self.worldNP = None
    self.world = None

  def feedValue(self, new_odometry):
    self.prev_odometry = self.odometry
    self.odometry = new_odometry

  def setup(self):
    self.worldNP = render.attachNewNode('World')

    # World
    self.debugNP = self.worldNP.attachNewNode(BulletDebugNode('Debug'))
    self.debugNP.show()
    self.debugNP.node().showWireframe(True)
    self.debugNP.node().showConstraints(False)
    self.debugNP.node().showBoundingBoxes(False)
    self.debugNP.node().showNormals(False)

    self.world = BulletWorld()
    self.world.setGravity(Vec3(0, 0, -9.81))
    self.world.setDebugNode(self.debugNP.node())

    # floor
    floor_shape = BulletBoxShape(Vec3(10, 10, 0.1))
    floor_body = BulletRigidBodyNode('Floor Body')
    floor_bodyNP = self.worldNP.attachNewNode(floor_body)
    floor_bodyNP.node().addShape(floor_shape)
    floor_bodyNP.setCollideMask(BitMask32.allOn())
    floor_bodyNP.setPos(-4, 0, -4)
    floor_bodyNP.setHpr(Vec3(0,0,-1))
    self.world.attachRigidBody(floor_body)

    # ceiling
    ceiling_shape = BulletBoxShape(Vec3(10, 10, 0.1))
    ceiling_body = BulletRigidBodyNode('Ceiling Body')
    ceiling_bodyNP = self.worldNP.attachNewNode(ceiling_body)
    ceiling_bodyNP.node().addShape(ceiling_shape)
    ceiling_bodyNP.setCollideMask(BitMask32.allOn())
    ceiling_bodyNP.setPos(-4, 0, 5)
    self.world.attachRigidBody(ceiling_body)

    # walls
    # back wall
    wall_shape = BulletBoxShape(Vec3(10, 0.1, 10))
    wall_body = BulletRigidBodyNode('Wall1 Body')
    wall_bodyNP = self.worldNP.attachNewNode(wall_body)
    wall_bodyNP.node().addShape(wall_shape)
    wall_bodyNP.setCollideMask(BitMask32.allOn())
    wall_bodyNP.setPos(-4, 4, 4)
    self.world.attachRigidBody(wall_body)
    # right wall
    wall_shape = BulletBoxShape(Vec3(0.1, 10, 10))
    wall_body = BulletRigidBodyNode('Wall2 Body')
    wall_bodyNP = self.worldNP.attachNewNode(wall_body)
    wall_bodyNP.node().addShape(wall_shape)
    wall_bodyNP.setCollideMask(BitMask32.allOn())
    wall_bodyNP.setPos(0, 0, 4)
    self.world.attachRigidBody(wall_body)
    # left wall
    wall_shape = BulletBoxShape(Vec3(0.1, 10, 10))
    wall_body = BulletRigidBodyNode('Wall3 Body')
    wall_bodyNP = self.worldNP.attachNewNode(wall_body)
    wall_bodyNP.node().addShape(wall_shape)
    wall_bodyNP.setCollideMask(BitMask32.allOn())
    wall_bodyNP.setPos(-8, 0, 4)
    self.world.attachRigidBody(wall_body)
    # front wall
    wall_shape = BulletBoxShape(Vec3(10, 0.1, 10))
    wall_body = BulletRigidBodyNode('Wall4 Body')
    wall_bodyNP = self.worldNP.attachNewNode(wall_body)
    wall_bodyNP.node().addShape(wall_shape)
    wall_bodyNP.setCollideMask(BitMask32.allOn())
    wall_bodyNP.setPos(-4, -4, 4)
    self.world.attachRigidBody(wall_body)

    # balls
    for i in range(15):
      ball_shape = BulletSphereShape(0.6)
      ball_body = BulletRigidBodyNode('Ball Body')
      ball_bodyNP = self.worldNP.attachNewNode(ball_body)
      ball_bodyNP.node().addShape(ball_shape)
      ball_bodyNP.node().setMass(1.0)
      ball_bodyNP.setCollideMask(BitMask32.allOn())
      ball_bodyNP.setPos(-2, 0, 0)
      self.world.attachRigidBody(ball_body)

    # wrist
    wrist_shape = BulletBoxShape(Vec3(0.5, 0.5, 0.5))
    wrist_body = BulletRigidBodyNode('Wrist Body')
    wrist_bodyNP = self.worldNP.attachNewNode(wrist_body)
    wrist_bodyNP.node().addShape(wrist_shape)
    wrist_bodyNP.node().setKinematic(True)
    wrist_bodyNP.setCollideMask(BitMask32.allOn())
    wrist_bodyNP.setPos(-8, 0, 0)
    self.world.attachRigidBody(wrist_body)
    self.wrist = wrist_bodyNP
    self.wrist_phys = wrist_body

    # elbow
    elbow_shape = BulletBoxShape(Vec3(0.5, 0.5, 0.5))
    elbow_body = BulletRigidBodyNode('Elbow Body')
    elbow_bodyNP = self.worldNP.attachNewNode(elbow_body)
    elbow_bodyNP.node().addShape(elbow_shape)
    elbow_bodyNP.node().setKinematic(True)
    elbow_bodyNP.setCollideMask(BitMask32.allOn())
    elbow_bodyNP.setPos(-4, 0, 0)
    self.world.attachRigidBody(elbow_body)
    self.elbow = elbow_bodyNP

    # shoulder
    shoulder_shape = BulletBoxShape(Vec3(0.5, 0.5, 0.5))
    shoulder_body = BulletRigidBodyNode('Shoulder Body')
    shoulder_bodyNP = self.worldNP.attachNewNode(shoulder_body)
    shoulder_bodyNP.node().addShape(shoulder_shape)
    shoulder_bodyNP.node().setKinematic(True)
    shoulder_bodyNP.setCollideMask(BitMask32.allOn())
    shoulder_bodyNP.setPos(0, 0, 0)
    self.world.attachRigidBody(shoulder_body)
    self.shoulder = shoulder_bodyNP

    # forearm
    forearm_shape = BulletCylinderShape(0.2, 2, 0)
    forearm_body = BulletRigidBodyNode('Forearm Body')
    forearm_bodyNP = self.worldNP.attachNewNode(forearm_body)
    forearm_bodyNP.node().addShape(forearm_shape)
    forearm_bodyNP.setCollideMask(BitMask32.allOn())
    forearm_bodyNP.setPos(-6, 0, 0)
    self.world.attachRigidBody(forearm_body)
    self.forearm = forearm_bodyNP

    # upper arm
    upper_arm_shape = BulletCylinderShape(0.2, 2, 0)
    upper_arm_body = BulletRigidBodyNode('Upper Arm Body')
    upper_arm_bodyNP = self.worldNP.attachNewNode(upper_arm_body)
    upper_arm_bodyNP.node().addShape(upper_arm_shape)
    upper_arm_bodyNP.setCollideMask(BitMask32.allOn())
    upper_arm_bodyNP.setPos(-2, 0, 0)
    self.world.attachRigidBody(upper_arm_body)
    self.upper_arm = upper_arm_bodyNP

if __name__ == "__main__":
 game = Game()
 run()