from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime
from math import sqrt
import scipy.misc
import time
import numpy as np
import PIL
import cv2
import sys
import multiprocessing
import os
import ctypes
import _ctypes
import pygame
import sys
import random
import sgc
import pickle
from PyKinectdataToPostgres import Kinect2Postgres
from random import randint

import pprint

JT_SpineBase = 0
JT_SpineMid = 1
JT_Neck = 2
JT_Head = 3
JT_ShoulderLeft = 4
JT_ElbowLeft = 5
JT_WristLeft = 6
JT_HandLeft = 7
JT_ShoulderRight = 8
JT_ElbowRight = 9
JT_WristRight = 10
JT_HandRight = 11
JT_HipLeft = 12
JT_KneeLeft = 13
JT_AnkleLeft = 14
JT_FootLeft = 15
JT_HipRight = 16
JT_KneeRight = 17
JT_AnkleRight = 18
JT_FootRight = 19
JT_SpineShoulder = 20
JT_HandTipLeft = 21
JT_ThumbLeft = 22
JT_HandTipRight = 23
JT_ThumbRight = 24
JT_Count = 25

if sys.hexversion >= 0x03000000:
    import _thread as thread
else:
    import thread


red = pygame.color.THECOLORS["red"]
blue = pygame.color.THECOLORS["blue"]
green = pygame.color.THECOLORS["green"]
black = (0,0,0)
white = (255,255,255)

lift_coach_logo = pygame.image.load("static/lift_coach_logo_40.png")

# colors for drawing different bodies
SKELETON_COLORS = [pygame.color.THECOLORS["red"],
                  pygame.color.THECOLORS["blue"],
                  pygame.color.THECOLORS["green"],
                  pygame.color.THECOLORS["orange"],
                  pygame.color.THECOLORS["purple"],
                  pygame.color.THECOLORS["yellow"],
                  pygame.color.THECOLORS["violet"]]


def calc_distance(joint_points,joint,x,y):
    joint_x = joint_points[joint].x
    joint_y = joint_points[joint].y
    distance=sqrt((joint_x-x)**2+(joint_y-y)**2)
    return distance

def calc_height(joint,fp):
    d,a,b,c = fp
    x,y,z= joint
    height = a*x + b*y + c*z + d
    return height

def clamp(n,lo,hi):
    if n < lo:
        return lo
    if n > hi:
        return hi
    return n


def calc_joint_angle(j):
    '''Takes 3 xyz np arrays and calculates the angle between them'''
    a = j[0]
    b = j[1]
    c = j[2]

    ba = a - b
    bc = c - b

    cosine_angle = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))
    angle = np.arccos(cosine_angle)

    return np.degrees(angle)

def joints_to_angle(joints,j1,j2,j3):
    j = joints_to_np(joints,j1,j2,j3)
    return calc_joint_angle(j)

def joints_to_np(joints,j1,j2,j3):
    a = joint_to_np(joints[j1])
    b = joint_to_np(joints[j2])
    c = joint_to_np(joints[j3])
    return np.array([a,b,c])

def joint_to_np(joint):
    return np.array([joint.Position.x,joint.Position.y,joint.Position.z])

def joint_to_tuple(joint):
    return((joint.Position.x,joint.Position.y,joint.Position.z))

def fp_to_tuple(plane):
    return((plane.w,plane.x,plane.y,plane.z))

def save_color_frame(fn,frame):
    start = time.clock()
    # pygame.image.save(target_surface,fn)  #10fps
    np_bgra = np.uint8(frame).reshape((1080,1920,4))
    b, g, r, a = cv2.split(np_bgra)
    img_RGB = cv2.merge((b[::2,::2], g[::2,::2], r[::2,::2]))
    cv2.imwrite(fn,img_RGB)

    elapsed = time.clock() - start
    print("save color frame fps = {}".format(elapsed*30))

def text_objects(text, font,color):
    textSurface = font.render(text, True, color)
    return textSurface, textSurface.get_rect()


def message_display_center(text,surface,x,y,size=12,color=(255,255,255)):
    largeText = pygame.font.Font('freesansbold.ttf',size)
    TextSurf, TextRect = text_objects(text, largeText,color)
    TextRect.center = (x,y)
    surface.blit(TextSurf, TextRect)

def message_display_topleft(text,surface,x,y,size=12,color=(255,255,255)):
    largeText = pygame.font.Font('freesansbold.ttf',size)
    TextSurf, TextRect = text_objects(text, largeText,color)
    TextRect.topleft = (x,y)
    surface.blit(TextSurf, TextRect)

class BodyGameRuntime(object):
    def __init__(self):

        if sys.platform == "win32":
            os.environ['SDL_VIDEODRIVER'] = 'directx'
        pygame.init()

        # Used to manage how fast the screen updates
        self._clock = pygame.time.Clock()
        self.init_time_stamp = int(time.time())

        # Set the width and height of the screen [width, height]
        self._infoObject = pygame.display.Info()
        screen_rez = (self._infoObject.current_w >> 1, self._infoObject.current_h >> 1)
        self._screen = pygame.display.set_mode(screen_rez, pygame.HWSURFACE|pygame.DOUBLEBUF|pygame.FULLSCREEN, 32)

        print(pygame.display.get_driver())
        pprint.pprint(pygame.display.Info())
        pygame.display.set_caption("Lift Coach")

        # Loop until the user clicks the close button.
        self._done = False

        # Used to manage how fast the screen updates
        self._clock = pygame.time.Clock()

        # Kinect runtime object, we want only color and body frames
        self._kinect = PyKinectRuntime.PyKinectRuntime(PyKinectV2.FrameSourceTypes_Color | PyKinectV2.FrameSourceTypes_Body | PyKinectV2.FrameSourceTypes_Infrared)

        #uniqueKinectId = self._kinect._sensor.uniqueKinectId(32)
        uniqueKinectId = "DarrellKinnect1"

        # back buffer surface for getting Kinect color frames, 32bit color, width and height equal to the Kinect color frame size
        self._frame_surface = pygame.Surface((self._kinect.color_frame_desc.Width, self._kinect.color_frame_desc.Height), 0, 32)

        # back buffer surface for getting Kinect color frames, 32bit color, width and height equal to the Kinect color frame size
        self._ui_surface = pygame.Surface((self._kinect.color_frame_desc.Width, self._kinect.color_frame_desc.Height), 0, 32)

        # here we will store skeleton data
        self._bodies = None
        self.ir_res = (self._kinect.infrared_frame_desc.Height, self._kinect.infrared_frame_desc.Width)
        self.color_res = (self._kinect.color_frame_desc.Height, self._kinect.color_frame_desc.Width)

        from multiprocessing import Pool
        self.pool = Pool(processes=4)

        self.floor_clip_plane  = (0,0,0,0)
        self.left_hand_pos  = (0,0,0)
        self.right_hand_pos  = (0,0,0)
        self.hand_width = []
        self.left_hand_height = []
        self.right_hand_height = []
        self.bar_height = []
        self.left_elb_angle = []
        self.right_elb_angle = []
        self.hip_angle = []
        self.left_knee_angle = []
        self.right_knee_angle = []
        self.timestamps = []


        self.k2p = Kinect2Postgres(uniqueKinectId,self.init_time_stamp)

    def draw_body_bone(self, joints, jointPoints, color, joint0, joint1, thickness = 10):

        if color is blue:
            thickness = 6

        if color is green and self.example_joint_points is not None:
            d1 = calc_distance(jointPoints,joint0, self.example_joint_points[joint0].x, self.example_joint_points[joint0].y)
            d2 = calc_distance(jointPoints,joint1, self.example_joint_points[joint1].x, self.example_joint_points[joint1].y)
            d =  d1+d2
            r = clamp(d,0,255)
            g = 255 - clamp(d,0,255)
            color = (r,g,0)
            self.total_error = clamp(self.total_error + d, 1, 9999)

        if joints is not None:
            joint0State = joints[joint0].TrackingState
            joint1State = joints[joint1].TrackingState

            # both joints are not tracked
            if (joint0State == PyKinectV2.TrackingState_NotTracked) or (joint1State == PyKinectV2.TrackingState_NotTracked):
                return

            # both joints are not *really* tracked
            if (joint0State == PyKinectV2.TrackingState_Inferred) and (joint1State == PyKinectV2.TrackingState_Inferred):
                return

        # ok, at least one is good
        start = (jointPoints[joint0].x, jointPoints[joint0].y)
        end = (jointPoints[joint1].x, jointPoints[joint1].y)

        try:
            pygame.draw.line(self._frame_surface, color, start, end, thickness)
        except: # need to catch it due to possible invalid positions (with inf)
            pass


        self.draw_body_bone_sideview( joints, jointPoints, color, joint0, joint1)


    def draw_body_bone_sideview(self, joints, jointPoints, color, joint0, joint1):

        if joints is  None:
            return

        x = 600
        y = 700
        scale = 200

        start = (joints[joint0].Position.z * -1 * scale + x, joints[joint0].Position.y * -1 * scale + y)
        end =   (joints[joint1].Position.z * -1 * scale + x, joints[joint1].Position.y * -1 * scale + y)

        try:
            pygame.draw.line(self._frame_surface, color, start, end, 8)
        except: # need to catch it due to possible invalid positions (with inf)
            pass

    def draw_error_bar(self, surf ):
        #(left, top, boxSize, boxSize)

        w = 1450
        s = clamp(self.total_error, 0, w*2)
        s = s/2

        d = s * (255/1450)

        r = clamp(d,0,255)
        g = 255 - clamp(d,0,255)
        color = (r,g,0)

        rr = (50, 830, s, 20)
        pygame.draw.rect(surf, color,  rr, 0)
        rr = (50, 830, w, 20)
        pygame.draw.rect(surf, (255,255,255),  rr, 2)



    def draw_black_bars(self,surf) :
        left_half = surf.get_rect()
        left_half.w = 400 #left_half.w/4 #1920/4 = 480
        darkness = 70
        surf.fill((darkness,darkness,darkness),left_half,special_flags=pygame.BLEND_RGB_SUB)
        top_bar = surf.get_rect()
        top_bar.h = 50
        top_bar.left = left_half.w
        surf.fill((darkness,darkness,darkness),top_bar,special_flags=pygame.BLEND_RGB_SUB)

    def draw_messages(self,surf) :
        '''shows messages on the left side of screen'''
        row_height = 45
        x = 20
        y = int(row_height * -0.5)

        red = (255,0,0)
        size = 46
        for message in self.messages.split('\n'):
            y += row_height
            message_display_topleft(message,surf,x,y,size,color=white)
            size=36
            # print(message)

    def draw_ball(self,color,x,y):
        pygame.draw.circle(self._frame_surface, color, (x,y), self.ball_size, 8)

    def draw_fade(self,surf):
        '''this blends the surface with black to proivde a fade effect '''
        #warning using this fade method degrades framerate
        surf.fill((10,10,10),special_flags=pygame.BLEND_RGB_SUB)

    def draw_body(self, joints, jointPoints, color):

        # Torso
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_Head, PyKinectV2.JointType_Neck)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_Neck, PyKinectV2.JointType_SpineShoulder)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_SpineShoulder, PyKinectV2.JointType_SpineMid)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_SpineMid, PyKinectV2.JointType_SpineBase)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_SpineShoulder, PyKinectV2.JointType_ShoulderRight)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_SpineShoulder, PyKinectV2.JointType_ShoulderLeft)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_SpineBase, PyKinectV2.JointType_HipRight)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_SpineBase, PyKinectV2.JointType_HipLeft)

        # Right Arm
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_ShoulderRight, PyKinectV2.JointType_ElbowRight)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_ElbowRight, PyKinectV2.JointType_WristRight)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_WristRight, PyKinectV2.JointType_HandRight)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_HandRight, PyKinectV2.JointType_HandTipRight)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_WristRight, PyKinectV2.JointType_ThumbRight)

        # Left Arm
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_ShoulderLeft, PyKinectV2.JointType_ElbowLeft)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_ElbowLeft, PyKinectV2.JointType_WristLeft)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_WristLeft, PyKinectV2.JointType_HandLeft)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_HandLeft, PyKinectV2.JointType_HandTipLeft)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_WristLeft, PyKinectV2.JointType_ThumbLeft)

        # Right Leg
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_HipRight, PyKinectV2.JointType_KneeRight)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_KneeRight, PyKinectV2.JointType_AnkleRight)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_AnkleRight, PyKinectV2.JointType_FootRight)

        # Left Leg
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_HipLeft, PyKinectV2.JointType_KneeLeft)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_KneeLeft, PyKinectV2.JointType_AnkleLeft)
        self.draw_body_bone(joints, jointPoints, color, PyKinectV2.JointType_AnkleLeft, PyKinectV2.JointType_FootLeft)

    def calc_body_angles(self,body):
        joints = body.joints
        self.left_hand_pos = joint_to_tuple(joints[PyKinectV2.JointType_HandLeft])
        self.right_hand_pos = joint_to_tuple(joints[PyKinectV2.JointType_HandRight])
        self.hand_width.append(np.linalg.norm(np.array(self.right_hand_pos)-np.array(self.left_hand_pos)))
        self.left_hand_height.append(calc_height(self.left_hand_pos,self.floor_clip_plane))
        self.right_hand_height.append(calc_height(self.right_hand_pos,self.floor_clip_plane))
        self.bar_height.append((self.right_hand_height[-1]+self.left_hand_height[-1])/2)
        self.left_elb_angle.append(joints_to_angle(joints,JT_ShoulderLeft,JT_ElbowLeft,JT_WristLeft))
        self.right_elb_angle.append(joints_to_angle(joints,JT_ShoulderRight,JT_ElbowRight,JT_WristRight))
        self.left_knee_angle.append(joints_to_angle(joints,JT_HipLeft,JT_KneeLeft,JT_AnkleLeft))
        self.right_knee_angle.append(joints_to_angle(joints,JT_HipRight,JT_KneeRight,JT_AnkleRight))
        self.timestamps.append(self.playtime)

        if self.show_debug_info:
            self.messages += "\nbar_height: {:.2f}".format(self.bar_height[-1])
            self.messages += "\nl_elbo_ang: {:.0f}".format(self.left_elb_angle[-1])
            self.messages += "\nr_elbo_ang: {:.0f}".format(self.right_elb_angle[-1])
            self.messages += "\nl_knee_ang: {:.0f}".format(self.left_knee_angle[-1])
            self.messages += "\nr_knee_ang: {:.0f}".format(self.right_knee_angle[-1])

        if(self.right_elb_angle[-1] < 160) or (self.left_elb_angle[-1] < 165):
            self.messages += "\nARMS STRAIGHT PLS"
        # self.messages += "\nhip_angle         = {:.0f}".format(self.hip_angle[-1])



    def draw_color_frame(self, frame, target_surface):
        target_surface.lock()
        address = self._kinect.surface_as_array(target_surface.get_buffer())
        ctypes.memmove(address, frame.ctypes.data, frame.size)
        del address
        target_surface.unlock()
        # fn = 'C:/tmp/kinect_frames/color_bgra{}.jpg'.format(self._kinect._last_color_frame_time)
        # save_color_frame(fn,frame)
        # result = self.pool.apply_async(save_color_frame, (fn,frame))
        # pygame.image.save(target_surface,fn)  #10fps,20 on intel


    def draw_infrared_frame(self, frame, target_surface):
        if frame is None:  # some usb hub do not provide the infrared image. it works with Kinect studio though
            return
        f8=np.uint8(frame.clip(1,4090)/16.)
        f8 = threshold(f8,252  )
        # blobs = blob_dog(np.reshape(f8,self.infrared_res), min_sigma=2, max_sigma=50, sigma_ratio=1.6, threshold=1.0, )
        blobs = blob_doh(np.reshape(f8,self.ir_res), min_sigma=3, max_sigma=5, threshold=0.03, num_sigma=2)


        if len(blobs) == 2:
            a = complex(blobs[0][0],blobs[0][1])
            b = complex(blobs[1][0],blobs[1][1])
            print ("bar len in px = {}".format(np.absolute(a-b)))

        for blob in blobs:
            y, x, r = blob
            print(r)
            pygame.draw.circle(target_surface,   pygame.color.THECOLORS["violet"], (x,y), 10, 2)


    def run(self):
        # -------- Main Program Loop -----------
        steps = ["1 Walk to the bar. Stand with feet hip width apart.",
        "2 Grab the bar, bending at the knees, back straight.",
        "3 Lift your chest. Pull. Take a big breath, and stand up.",
        "4 Great Job, Drop bar and Victory Pose! ",
        ]

        example_joints = None
        self.example_joint_points = None
        cur_step = 0
        self.playtime = 0.0
        framecount = -1
        self.show_example_body = True
        self.show_color_frame = True
        self.show_debug_info = False
        self.total_error = 9999
        self.error_threshold = 1000
        self.go_frames = 0
        self.reps = 0
        self.heartrate = 94

        #gross init hack
        with open('example{}.pickle'.format(cur_step), 'rb') as f:
            example_joints, self.example_joint_points = pickle.load(f)
            example_joints = None

        while not self._done:

            self.messages = "\n\n\n"
            if self.show_example_body:
                self.messages += "\nBE THE BLUE"
            self.messages += "\nUSER: DARRELL"
            self.messages += "\nMODE: DEADLIFT"
            self.messages += "\nREPS: {}".format(self.reps)
            self.messages += "\nWEIGHT: 25LBS"
            if self.show_debug_info:
                self.messages += "\nFPS: {0:.2f} \nPlaytime: {1:.2f}".format(self._clock.get_fps(), self.playtime)

            #we want to be good for at least a few frames
            if self.total_error < self.error_threshold:
                framecount += 1
            elif framecount > 0:
                framecount -= 1

            #we want to be good for at least a few frames
            if framecount >= 7:
                framecount = 0
                cur_step = cur_step + 1
                cur_step = cur_step % len(steps)
                if cur_step == len(steps)-1:
                    self.reps += 1
                with open('example{}.pickle'.format(cur_step), 'rb') as f:
                    example_joints, self.example_joint_points = pickle.load(f)
                    example_joints = None

            # --- Main event loop
            for event in pygame.event.get(): # User did something
                if event.type == pygame.QUIT: # If user clicked close
                    self._done = True # Flag that we are done so we exit this loop

                elif event.type == pygame.VIDEORESIZE: # window resized
                    self._screen = pygame.display.set_mode(event.dict['size'],
                                               pygame.HWSURFACE|pygame.DOUBLEBUF|pygame.RESIZABLE, 32)

                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        self._done = True # Flag that we are done so we exit this loop
                    if event.key == pygame.K_q:
                        self._done = True # Flag that we are done so we exit this loop
                    if event.key == pygame.K_s:
                        with open('example{}.pickle'.format(cur_step), 'wb') as f:
                            pickle.dump((self.last_joints,self.last_joint_points), f, pickle.HIGHEST_PROTOCOL)
                    if event.key == pygame.K_e:
                        self.show_example_body = not self.show_example_body
                    if event.key == pygame.K_c:
                        self.show_color_frame = not self.show_color_frame
                    if event.key == pygame.K_d:
                        self.show_debug_info = not self.show_debug_info
                    if event.key == pygame.K_n:
                        framecount = 100 #skip to next pose


            # --- Game logic should go here

            # --- Getting frames and drawing
            # --- Woohoo! We've got a color frame! Let's fill out back buffer surface with frame's data
            # for now we allways draw the color frame to stabilze framerate
            if self._kinect.has_new_color_frame() or True:
                frame = self._kinect.get_last_color_frame()
                if self.show_color_frame:
                    self.draw_color_frame(frame, self._frame_surface)
                frame = None

            # --- Cool! We have a body frame, so can get skeletons
            if self._kinect.has_new_body_frame():
                self._bodies = self._kinect.get_last_body_frame()
                # print(self._bodies.bodies[0].size )

            if not self.show_color_frame:
                self.draw_fade(self._frame_surface)
            self.draw_black_bars(self._frame_surface)
            #show the text for the current step
            message_display_topleft(steps[cur_step],self._frame_surface,410,5,size=40,color=(255,255,255))

            if self.example_joint_points is not None and self.show_example_body:
                self.draw_body(example_joints, self.example_joint_points, blue)


            # --- draw skeletons to _frame_surface
            if self._bodies is not None:
                self.floor_clip_plane = fp_to_tuple(self._bodies.floor_clip_plane)
                for i in range(0, self._kinect.max_body_count):
                    body = self._bodies.bodies[i]
                    if not body.is_tracked:
                        continue
                    self.total_error = 1
                    self.calc_body_angles(body)
                    # self.k2p.save_joints(body,self._kinect._last_body_frame_time)
                    joints = body.joints

                    # convert joint coordinates to color space
                    joint_points = self._kinect.body_joints_to_color_space(joints)
                    self.draw_body(joints, joint_points, SKELETON_COLORS[2])
                    self.last_joint_points = joint_points

                    joint_dicts = []
                    for index in range(PyKinectV2.JointType_Count):
                        joint_dicts.append(self.k2p.joint_to_dict(body.joints[index],body.joint_orientations[index],index,0))
                    self.last_joints = joint_dicts
                    if randint(0,10) == 9:
                        self.heartrate += randint(-1,1)
                    self.heartrate = clamp(self.heartrate, 77, 117)
                    self.messages += "\nHEARTRATE: {}".format(self.heartrate)

            if self.show_debug_info:
                self.messages += "\ntotal_error: {:.0f}".format(self.total_error)

            self.draw_error_bar(self._frame_surface)
            self.draw_messages(self._frame_surface)

            #draw logo
            self._frame_surface.blit(lift_coach_logo, (0,0))
            # --- copy back buffer surface pixels to the screen, resize it if needed and keep a aspect ratio
            # --- (screen size may be different from Kinect's color frame size)
            if False:
                h_to_w = float(self._frame_surface.get_height()) / self._frame_surface.get_width()
                target_height = int(h_to_w * self._screen.get_width())
                surface_to_draw = pygame.transform.scale(self._frame_surface, (self._screen.get_width(), target_height))
                self._screen.blit(surface_to_draw, (0,0))
            else:
                self._screen.blit(self._frame_surface, (0,0))
            surface_to_draw = None
            # pygame.display.set_caption(" LIFT COACH FPS: {0:.2f}  Playtime: {1:.2f} ".format(self._clock.get_fps(), self.playtime))
            # pygame.display.update()

            # --- Go ahead and update the screen with what we've drawn.
            pygame.display.flip()

            # --- Limit to 60 frames per second
            milliseconds = self._clock.tick(60)
            self.playtime += milliseconds / 1000.0

        # Close our Kinect sensor, close the window and quit.
        self._kinect.close()
        pygame.quit()



if __name__ == '__main__':
    # multiprocessing.freeze_support()
    __main__ = "Kinect v2 Body Game"
    game = BodyGameRuntime()
    game.run()

