from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime
from scipy.stats import threshold
# from skimage import data, io, filters   
# from skimage.feature import blob_dog, blob_log, blob_doh
from math import sqrt

import triangle


# from skimage.color import rgb2gray
import cv2

import matplotlib.pyplot as plt



import ctypes
import _ctypes
import pygame
import sys
import numpy as np


if sys.hexversion >= 0x03000000:
    import _thread as thread
else:
    import thread

# colors for drawing different bodies 
SKELETON_COLORS = [pygame.color.THECOLORS["red"],
                    pygame.color.THECOLORS["blue"], 
                    pygame.color.THECOLORS["green"],
                    pygame.color.THECOLORS["orange"], 
                    pygame.color.THECOLORS["purple"], 
                    pygame.color.THECOLORS["yellow"], 
                    pygame.color.THECOLORS["violet"]]



def auto_canny(image, sigma=0.33, lower=None, upper=None, debug=False):
    # compute the median of the single channel pixel intensities
    v = np.median(image)
    # apply automatic Canny edge detection using the computed median
    if lower is None:
        lower = int(max(0, (1.0 - sigma) * v))
    if upper is None:
        upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)
    if debug:
        print((v,lower,upper))
    # return the edged image
    return edged

class InfraRedRuntime(object):
    def __init__(self):
        pygame.init()

        # Used to manage how fast the screen updates
        self._clock = pygame.time.Clock()

        # Loop until the user clicks the close button.
        self._done = False

        # Used to manage how fast the screen updates
        self._clock = pygame.time.Clock()

        # Kinect runtime object, we want only color and body frames 
        self._kinect = PyKinectRuntime.PyKinectRuntime(PyKinectV2.FrameSourceTypes_Depth | PyKinectV2.FrameSourceTypes_Infrared)

        # back buffer surface for getting Kinect infrared frames, 8bit grey, width and height equal to the Kinect color frame size
        self._depth_surface = pygame.Surface((self._kinect.depth_frame_desc.Width, self._kinect.depth_frame_desc.Height), 0, 24)
        self._ir_surface = pygame.Surface((self._kinect.depth_frame_desc.Width, self._kinect.depth_frame_desc.Height), 0, 24)
        self._bar_surface = pygame.Surface((self._kinect.depth_frame_desc.Width, self._kinect.depth_frame_desc.Height), 0, 24)
        # here we will store skeleton data 
        self._bodies = None
        
        # Set the width and height of the screen [width, height]
        self._infoObject = pygame.display.Info()
        self._screen = pygame.display.set_mode((self._kinect.depth_frame_desc.Width*2, self._kinect.depth_frame_desc.Height*2), 
                                                pygame.HWSURFACE|pygame.DOUBLEBUF|pygame.RESIZABLE, 32)

        print("infrared res = {}x{}".format(self._kinect.depth_frame_desc.Width, self._kinect.depth_frame_desc.Height))
        self.infrared_res = (self._kinect.depth_frame_desc.Width, self._kinect.depth_frame_desc.Height)
        self.infrared_res = (self._kinect.depth_frame_desc.Height, self._kinect.depth_frame_desc.Width)

        pygame.display.set_caption("Kinect for Windows v2 depth")

        self.depth8 = None
        self.ir8 = None

    def draw_infrared_frame(self, frame, target_surface):
        if frame is None:  # some usb hub do not provide the infrared image. it works with Kinect studio though
            return
        target_surface.lock()
        f8 = np.uint8(frame.clip(1, 4095) / 16.)
        self.ir8 = np.reshape(f8,self.infrared_res)
        frame8bit = np.dstack((f8, f8, f8))
        address = self._kinect.surface_as_array(target_surface.get_buffer())
        ctypes.memmove(address, frame8bit.ctypes.data, frame8bit.size)
        del address
        target_surface.unlock()




    def draw_bar_frame(self,  target_surface):
        if self.ir8 is None or self.depth8 is None:
            return
        min_dist = 100
        max_dist = 140
        t8 = threshold(self.depth8,100,140 )
        ir8 = threshold(self.ir8,255,256).clip(0,1)
        b8 = ir8 * t8       
        blur = cv2.GaussianBlur(b8,(3,3),0)
        
        # edges = auto_canny(blur,lower=6,upper=10)
        edges = auto_canny(blur,debug=True)
        
        #     cv2.line(img,(x1,y1),(x2,y2),(0,0,255),2)
        # frame8bit=np.dstack((f8,f8,f8)) 
        frame8bit=np.dstack((blur,edges,b8))

        params = cv2.SimpleBlobDetector_Params()
        params.blobColor=255
        # Change thresholds
        params.minThreshold = 128
        params.maxThreshold = 255
        # Filter by Area.
        params.filterByArea = True
        params.minArea = 20
        params.maxArea = 200
        # Filter by Circularity
        params.filterByCircularity = False
        params.minCircularity = 0.1
        # Filter by Convexity
        params.filterByConvexity = False
        params.minConvexity = 0.87
        # Filter by Inertia
        params.filterByInertia = False
        params.minInertiaRatio = 0.01
        detector = cv2.SimpleBlobDetector_create(params)
        keypoints = detector.detect(edges)

        address = self._kinect.surface_as_array(target_surface.get_buffer())
        ctypes.memmove(address, frame8bit.ctypes.data, frame8bit.size )
        del address
        x = []
        y = []
        for blob in keypoints:
            x.append(blob.pt[0])
            y.append(blob.pt[1])
            r = blob.size * blob.size * 3.14
            # print(r)
            pygame.draw.circle(target_surface, pygame.color.THECOLORS[ "violet"], (int(x[-1]), int(y[-1])), 10, 2)

        if len(keypoints) == 2:
            color = (255,255,255)
            
            # pygame.draw.line(target_surface, color,  (x[0],y[0]), (x[1],y[1]), 4)

        l_mean = np.array(edges[10:414,:255].nonzero()).mean(axis=1)[::-1]
        r_mean = np.array(edges[10:414,255:].nonzero()).mean(axis=1)[::-1]
        r_mean[0] += 254
        r_mean[1] += 10
        l_mean[1] += 10
        # pygame.draw.circle(target_surface, pygame.color.THECOLORS["blue"], l_mean, 20, 3)
        try:
            pygame.draw.line(target_surface, (255,0,0),  l_mean, r_mean, 2)
        except:
            print(l_mean,r_mean)
                    
        target_surface.unlock()


    def draw_depth_frame(self, frame, target_surface):
        target_surface.lock()
        f8=np.uint8(frame.clip(1,4095)/16.)
        #     cv2.line(img,(x1,y1),(x2,y2),(0,0,255),2)
        self.depth8 = np.reshape(f8,self.infrared_res)
        # frame8bit=np.dstack((f8,f8,f8)) 
        frame8bit=np.dstack((f8,f8,f8)) 
        # frame8bit=np.dstack((edges,edges,edges)) 
        address = self._kinect.surface_as_array(target_surface.get_buffer())
        ctypes.memmove(address, frame8bit.ctypes.data, frame8bit.size)
        del address 
        target_surface.unlock()

    def run(self):
        playtime = 0    
        # -------- Main Program Loop -----------
        while not self._done:
            # --- Main event loop
            for event in pygame.event.get(): # User did something
                if event.type == pygame.QUIT: # If user clicked close
                    self._done = True # Flag that we are done so we exit this loop

                elif event.type == pygame.VIDEORESIZE: # window resized
                    self._screen = pygame.display.set_mode(event.dict['size'], 
                                                pygame.HWSURFACE|pygame.DOUBLEBUF|pygame.RESIZABLE, 32)

                elif event.type == pygame.MOUSEBUTTONDOWN:
                    pos = pygame.mouse.get_pos()
                    if self.depth8 is not None:
                        py = pos[1]%424 #modulo height
                        px = pos[0]%512 #modulo width 
                        depth = self.depth8[py,px]
                        bright = self.ir8[py,px]
                        text = "pos = {} depth = {} bright= {}".format(pos,depth,bright)
                        pygame.display.set_caption(text)
            # print self._clock.get_fps()

            # --- Getting frames and drawing  
            if self._kinect.has_new_depth_frame():
                frame = self._kinect.get_last_depth_frame()
                self.draw_depth_frame(frame, self._depth_surface)
                frame = None
            self._screen.blit(self._depth_surface, (0,0))

            # --- Getting frames and drawing
            if self._kinect.has_new_infrared_frame():
                frame = self._kinect.get_last_infrared_frame()
                self.draw_infrared_frame(frame, self._ir_surface)
                frame = None
            self._screen.blit(self._ir_surface, (512,0))

            self.draw_bar_frame(self._bar_surface)
            self._screen.blit(self._bar_surface, (512,424))

            pygame.display.update()

            # --- Go ahead and update the screen with what we've drawn.
            pygame.display.flip()

            # --- Limit to 60 frames per second            
            milliseconds = self._clock.tick(60)
            playtime += milliseconds / 1000.0


        # Close our Kinect sensor, close the window and quit.
        self._kinect.close()
        pygame.quit()


__main__ = "Kinect v2 InfraRed"
game =InfraRedRuntime()
game.run()

