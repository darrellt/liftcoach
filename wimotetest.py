#! /usr/bin/python

'''Try to implement the example in python'''

import wiiuse
import sys
import time
import os
import bluetooth

nmotes = 1

print("performing inquiry...")

nearby_devices = bluetooth.discover_devices(
        duration=1, lookup_names=True, flush_cache=True, lookup_class=False)


print("found %d devices" % len(nearby_devices))

for addr, name in nearby_devices:
    try:
        print("  %s - %s" % (addr, name))
    except UnicodeEncodeError:
        print("  %s - %s" % (addr, name.encode('utf-8', 'replace')))


def handle_event(wmp):
    wm = wmp[0]
    # print('--- EVENT [wiimote id %i] ---' % wm.unid)
    print('battery_level = {}'.format(wm.battery_level))    
    print('dev_handle= {}'.format(wm.dev_handle))    
    # print('bdaddr= {}'.format(wm.bdaddr))    
    if wm.btns:
        for name, b in list(wiiuse.button.items()):
            if wiiuse.is_pressed(wm, b):
                print(name,'pressed')

   #button A and player 1 led are accessable on wiiboard 

    if wm.exp.type == wiiuse.EXP_WII_BOARD:
        wb = wm.exp.u.board
        
        print('wb tl = %f' % wb.tl)
        print('wb tr = %f' % wb.tr)
        print('wb bl = %f' % wb.bl)
        print('wb br = %f' % wb.br)


address = '00:23:31:90:80:12'

if os.name != 'nt': print('Press 1&2')

wiimotes = wiiuse.init(nmotes)
found = wiiuse.find(wiimotes, nmotes, 5)
if not found:
    print('not found')
    sys.exit(1)

connected = wiiuse.connect(wiimotes, nmotes)
if connected:
    print('Connected to %i wiimotes (of %i found).' % (connected, found))
else:
    print('failed to connect to any wiimote.')
    sys.exit(1)

for i in range(nmotes):
    wiiuse.set_leds(wiimotes[i], wiiuse.LED[i])

try:
    loops = 0
    while loops < 3:
        r = wiiuse.poll(wiimotes, nmotes)
        if r != 0:
            handle_event(wiimotes[0])
        loops += 1

    import ipdb
    # ipdb.set_trace()
except KeyboardInterrupt:
    pass

for i in range(nmotes):
    wiiuse.set_leds(wiimotes[i], 0)
    wiiuse.disconnect(wiimotes[i])

print('done')

